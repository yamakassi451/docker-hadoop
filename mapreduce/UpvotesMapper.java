package SalesCountry;

import java.io.IOException;

import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.*;

public class UpvotesMapper extends MapReduceBase implements Mapper<LongWritable, Text, Text, DoubleWritable> {

    private boolean isFirstLine = true;

    public void map(LongWritable key, Text value, OutputCollector<Text, DoubleWritable> output, Reporter reporter) throws IOException {

        String valueString = value.toString();

        if (isFirstLine) {
            isFirstLine = false;
            return; 
        }

        String[] UpvotesData = valueString.split(",");
        
        if (UpvotesData.length == 14) {
            try {
                double totalProfit = Double.parseDouble(UpvotesData[7]);
                output.collect(new Text(UpvotesData[8]), new DoubleWritable(totalProfit));
            } catch (NumberFormatException e) {
                System.out.println(UpvotesData[8]);
              }
        }
    }
}

  