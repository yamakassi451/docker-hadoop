#!/usr/bin/env python

from operator import itemgetter 
import sys

# keep a map of the sum of upvotes of each reddit
redditupvotemap = {}

for line in sys.stdin:
    line = line.strip()
    reddit, count = line.split('\t', 1)
    try:
        count = int(count)
        redditupvotemap[reddit] = redditupvotemap.get(reddit, 0) + count
    except ValueError:
        # ignore lines where the count is not a number
        pass

# sort the reddits alphabetically;
alphabetic_redditupvotemap = sorted(redditupvotemap.items(), key=itemgetter(0))

# output to STDOUT
for reddit, countupvotes in alphabetic_redditupvotemap:
    print '%s\t%s'% (reddit, countupvotes)