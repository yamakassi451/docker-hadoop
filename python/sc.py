import socket
import time


server_address = ('localhost', 9999)

server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

server_socket.bind(server_address)

server_socket.listen(1)

print(f"server listen {server_address}")

try:
    client_socket, client_address = server_socket.accept()
    print(f"client connect{client_address}")
    
    while True:
        message = "hello"
        client_socket.send(message.encode('utf-8'))
        time.sleep(1)  

except Exception as e:
    print(f"error: {e}")
finally:
    server_socket.close()
