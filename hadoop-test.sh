#!/bin/bash

if [ ! -d "lab1" ]; then
  mkdir lab1
fi

hdfs dfs -mkdir /lab01

if [ $? -ne 0 ]; then
  echo "Директория уже создана"
  exit 1
fi

cd lab1
echo "test test" > lab.txt
cat lab.txt

hdfs dfs -put lab.txt /lab01

if [ $? -ne 0 ]; then
  echo "Не удалось поместить файл в HDFS"
  exit 1
fi

hdfs dfs -cat /lab01/lab.txt

if [ $? -ne 0 ]; then
  echo "Не удалось прочитать файл из HDFS"
  exit 1
fi

echo "Скрипт успешно завершен"


###
hdfs dfs -setrep 3 /lab01/lab.txt

# hdfs dfs -rm /lab01/lab.txt
#
#